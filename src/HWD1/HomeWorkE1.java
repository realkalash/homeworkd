package HWD1;

public class HomeWorkE1 {

    public static void main(String[] args) {
        Book book = new Book();

        String intoAuthor       =   "Поиск по автору";
        String intoYear         =   "Поиск по году(Будут выведены все варианты ранее этого года)";
        String intoNameBook     =   "Поиск по названию книги";
        String intoOlderBook    =   "Поиск самой старой книги";
        String intoCount        =   "Вывести книги с 3 и более авторами";

        book.GenerateBooks();

        System.out.println("Выберите один из предложеных вариантов: ");
        System.out.println(intoAuthor + "\n" + intoYear + "\n" + intoNameBook+"\n"+intoCount);

        book.ScanResult(intoAuthor, intoYear, intoNameBook, intoOlderBook, intoCount);
    }
}
