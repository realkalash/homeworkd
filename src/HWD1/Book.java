package HWD1;

import java.util.Scanner;

public class Book {
    String autor;
    String nameBook;
    int year;

    String[] autors = {"Достоевский", "Толстой", "Гоголь", "Горький", "А.И Витя, В.И Валера, Ержан"};
    String[] names = {"Преступление и наказание", "Война и мир", "Мертвые души", "Мать", "Баллада о дураках"};
    String[] publisher = {"Солнышко", "Скучные истории", "Energizer", "Зёрнышко", "Народ"};
    int[] years = {1842, 1866, 1869, 1906, 2017};
    Book[] books = new Book[autors.length];
    String str;

    public void GenerateBooks() {
        for (int i = 0; i < books.length; i++) {
            Book book = new Book();
            book.autor = autors[i];
            book.year = years[i];
            book.nameBook = names[i];
            books[i] = book;
        }
    }

    public void ScanResult(String intoAuthor, String intoYear, String intoNameBook, String intoOlderBook, String intoCount) {
        Scanner into = new Scanner(System.in);
        str = into.nextLine().toLowerCase();

        if (intoAuthor.toLowerCase().contains(str.toLowerCase()) || str.equals("1")) {

            System.out.println("Введите автора: ");
            str = into.nextLine().toLowerCase();

            for (int i = 0; i < books.length; i++) {
                if (str.equals(autors[i].toLowerCase()) || autors[i].toLowerCase().contains(str.toLowerCase())) {
                    System.out.println(String.format("Автор: %s \nНазвание книги: %s \nГод издания: %s\nИздатель: %s", autors[i], names[i], years[i], publisher[i]));
                }
            }
        } else if (str.equals(intoYear.toLowerCase()) || str.equals("2")) {

            System.out.println("Введите год: ");
            str = into.nextLine();
            int parseInt = Integer.parseInt(str);

            for (int i = 0; i < books.length; i++) {
                if (years[i] <= parseInt) {
                    System.out.println(String.format("Автор: %s \nНазвание книги: %s \nГод издания: %s\nИздатель: %s", autors[i], names[i], years[i], publisher[i]));
                }
            }
        } else if (intoNameBook.toLowerCase().contains(str.toLowerCase()) || str.equals("3")) {

            System.out.println("Введите название: ");
            str = into.nextLine().toLowerCase();
            for (int i = 0; i < books.length; i++) {
                if (str.equals(names[i].toLowerCase())) {
                    System.out.println(String.format("Автор: %s \nНазвание книги: %s \nГод издания: %s\nИздатель: %s", autors[i], names[i], years[i], publisher[i]));
                }
            }
        } else if (intoOlderBook.toLowerCase().contains(str.toLowerCase()) || str.equals("4")) {
            Book oldestBook = books[0];
            for (int i = 0; i < books.length; i++) {
                if (oldestBook.year > books[i].year) {
                    oldestBook = books[i];
                    System.out.println("Самая старая книга это" + oldestBook);
                }
            }
        } else if (intoCount.toLowerCase().contains(str.toLowerCase()) || str.equals("5")) {
            System.out.println("Книги в которых 3 и более авторов: \n");

            for (int i = 0; i < books.length; i++) {
                int countComa = autors[i].length() - autors[i].replace(",", "").length() + 1;
                if (countComa >= 3) {
                    System.out.println(String.format("Автор: %s \nНазвание книги: %s \nГод издания: %s\nИздатель: %s", autors[i], names[i], years[i], publisher[i]));
                }
            }
        }
    }
}

